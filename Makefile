old:
	g++ -c src/main.cpp -DSFML_STATIC \
	-Iinclude -Llib -lsfml-graphics -lsfml-system -lsfml-window -lopengl32 -lwinmm -lgdi32 -lfreetype -lstdc++
obj:	
	g++ -c src/Object.cpp -DSFML_STATIC \
	-Iinclude -Llib -lsfml-graphics -lsfml-system -lsfml-window -lopengl32 -lwinmm -lgdi32 -lfreetype -lstdc++
Lis:
	g++ -c src/Listener.cpp -DSFML_STATIC \
	-Iinclude -Llib -lsfml-graphics -lsfml-system -lsfml-window -lopengl32 -lwinmm -lgdi32 -lfreetype -lstdc++
open:
	g++ -c src/Openfile.cpp


gc:
	g++ src/main.cpp src/Object.cpp src/Openfile.cpp src/Listener.cpp -o bin/main.exe -DSFML_STATIC \
	-Iinclude -Llib -lsfml-graphics -lsfml-system -lsfml-window -lopengl32 -lwinmm -lgdi32 -lfreetype -lstdc++


clean:
	rm -rf Object.out main.o

