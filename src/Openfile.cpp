#include "Openfile.h"

namespace dat {
int loadMap (std::string numberMap, char **s) {
    if (s == NULL) {
        return -1;
    }

    std::ifstream in;
    in.open (numberMap, std::ifstream::in);

    for (int i = 0; i < 30; i++) {
        for (int j = 0; j < 38; j++) {
            in.get(s[i][j]);
        }
        in.get();
    }

    in.close();

    return 0;
}

int saveData(std::string nameFile, std::string data) {
    std::ifstream in;
    in.open(nameFile, std::ifstream::in);

    in >> data;

    in.close();

    return 0;
}

int readData(std::string nameFile, std::string &data) {
    std::ofstream os;

    os.open(nameFile, std::ofstream::out);
    os << data;

    os.close();

    return 0;
}

int getLevel(int *lev) {
    std::string l;
    readData("level.dat", l);

    *lev = std::stoi(l);

    return 0;
}

void delData(std::string nameFile) {
    std::ofstream os;
    os.open(nameFile, std::ofstream::trunc);
    os.close();
}

void getName(std::string &name) {
    readData("name.dat", name);
}

void setName(std::string name) {
    delData("name.dat");
    saveData("name.dat", name);
}

} // namespace
