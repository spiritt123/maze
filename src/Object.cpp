
#include "Object.h"
#include "Listener.h"
const int W = 19; //Weidth
const int H = 15; //Height

double Object::getX(){
		return x;
	}

double Object::getY(){
		return y;
	}

void Object::setSprite(Sprite sp){
		this->sprite = sp;
	}

Sprite Object::getSprite(){
		return sprite;
	}

void Object::setCoord(double x, double y){
		this->x = x;
		this->y = y;
	}

Player::Player(Texture &image) {
		Vector2f targetSize(20.0f, 20.0f);

		sprite.setTexture(image);
		sprite.setPosition(rect.left, rect.top);
		sprite.setScale(targetSize.x / 13 * 2, targetSize.y / 13 * 2);
		sprite.setTextureRect(IntRect(0, 2, 13, 13));
		x = y = 0;
		currentFrame = 0;
		nap = 0;
		rect.left = 20;
		rect.top = 0;
	}

void Player::Update(float time) {
		
		rect.left += x * time;
		rect.top += y * time;

		Vector2f targetSize(20.0f, 20.0f);

		if (nap != 4)currentFrame += 0.005*time;
		if (currentFrame > 2 )currentFrame -= 2;
		switch (nap){
			case 0: 
				sprite.setTextureRect(IntRect(0 + 16 * (int)currentFrame, 0, 13, 13));
				break;
			case 1: 
				sprite.setTextureRect(IntRect(45 + 16 * (int)currentFrame, 0, -13, 13));
				break;
			case 2: 
				sprite.setTextureRect(IntRect(0 + 16 * (int)currentFrame, 13, 13, -13));
				break;
			case 3: 
				sprite.setTextureRect(IntRect(32 + 16 * (int)currentFrame, 0, 13, 13));
				break;
		}
		sprite.setScale(targetSize.x / 13 * 2, targetSize.y / 13 * 2);
		sprite.setPosition(rect.left, rect.top);
	}

Block::Block(){

		x = 0;
		y = 0;
		Collision = true;		
	}

Block::Block(Texture &image, char Map[30][38], Listener Ls){
		Vector2f targetSize(20.0f, 20.0f);
		
		for (int i = 0; i < 30; i++) {
	       	for (int j = 0; j < 38; j++){
	       		if (Map[i][j] != ' '){
					map_s[i][j].setTexture(image);
					map_s[i][j].setPosition(j*20, i*20);
					map_s[i][j].setScale(targetSize.x / 8, targetSize.y / 8);
					Ls.Add(j*20, i*20, 20, 20);
				}
				if (Map[i][j] == '~'){
		            map_s[i][j].setTextureRect(IntRect(128, 8, 8, 8));
				}
				if (Map[i][j] == '#'){
		            map_s[i][j].setTextureRect(IntRect(128, 0, 8, 8));
				}
				if (Map[i][j] == '-'){
		            map_s[i][j].setTextureRect(IntRect(136, 8, 8, 8));
				}
				if (Map[i][j] == 'x'){
		            map_s[i][j].setTextureRect(IntRect(144, 0, 8, 8));
				}
				if (Map[i][j] == '0'){
		            map_s[i][j].setTextureRect(IntRect(144, 8, 8, 8));
				}
	       	}
	    }
	}

Bot::Bot(){

		x = 0;
		y = 0;
		Collision = true;		
	}

Bot::Bot(double x, double y, bool col){

		this->x = x;
		this->y = y;		
		this->Collision = col;		
	}

void Bot::Go(){
/*
	double VerX = W - getX;
	double VerY = H*2 - getY;

	if(VerX < VerY){
		
	}else{
		
	}*/

	}

