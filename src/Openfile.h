//#ifndef INCLUDE_OPENFILE_H_
//#define INCLUDE_OPENFILE_H_
#pragma once

#include <iostream>
#include <string>
#include <fstream>

namespace dat {
    int loadMap (std::string numberMap, char **s);
    int getLevel(int *lev);
    void getName(std::string &name);
    void setName(std::string name);
}

//#endif  //  INCLUDE_OPENFILE_H_

