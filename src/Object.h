#ifndef OBJECT_H
#define OBJECT_H

#include "Listener.h"
#include <SFML/Graphics.hpp>
using namespace sf;

class Object{

public: 

	double getX();

	double getY();

	void setSprite(Sprite sp);

	Sprite getSprite();

	void setCoord(double x, double y);

protected:

	double x,y;
	bool Collision;
	Sprite sprite;

};

class Player : public Object{
public:

	FloatRect rect;
	float currentFrame;
	int nap;
	
	Player(Texture &image);

	void Update(float time);

};

class Block : public Object{

public:
	Sprite map_s[30][38];

	Block();

	Block(Texture &image, char map[30][38], Listener Ls);



};

class Bot : public Object{

public:

	Bot();

	Bot(double x, double y, bool col);

	void Go();//IA

};


#endif //OBJECT_H